package tcmb.webservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tcmb.webservice.domain.model.response.ExchangeRateResponse;
import tcmb.webservice.service.ExchangeRateService;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ExchangeRateController {

    private final ExchangeRateService exchangeRateService;

    @GetMapping("/get")
    public ResponseEntity<ExchangeRateResponse> getExchangeRates(String series,
                                                                 String startDate,
                                                                 String endDate,
                                                                 String type){
        var data = exchangeRateService.getExchangeRates(
                "TP.DK.USD.S.YTL",
                "01-01-2024",
                "01-02-2024",
                "1",
                "json");
        return ResponseEntity.ok(ExchangeRateResponse.builder()
                        .totalCount(data.getTotalCount())
                        .items(data.getItems())
                        .build());
    }
}
