package tcmb.webservice.domain.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum Formula {

    LEVEL(0),
    PERCENT_CHANGE(1),
    DIFFERENCE(2),
    ANNUAL_PERCENTAGE(3),
    ANNUAL_DIFFERENCE(4),
    PERCENT_CHANGE_END_PREVIOUS_YEAR(5),
    DIFFERENCE_COMPARED_END_PREVIOUS_YEAR(6),
    MOVING_AVERAGE(7),
    MOVING_TOTAL(8);

    private final Integer formulaCode;

    public static Integer fromFormula(String formula){
        return Stream.of(values())
                .filter(value-> value.name().equals(formula))
                .map(Formula::getFormulaCode)
                .findFirst()
                .orElse(0);
    }
}
