package tcmb.webservice.domain.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRateResponse {

    @JsonProperty("totalCount")
    private Integer totalCount;
    @JsonProperty("items")
    private List<ItemDTO> items;
}
