package tcmb.webservice.domain.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ItemDTO {

    @JsonProperty("Tarih")
    String date;

    @JsonProperty("TP_DK_USD_S_YTL")
    String series;
}
