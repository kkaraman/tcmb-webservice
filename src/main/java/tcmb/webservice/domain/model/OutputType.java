package tcmb.webservice.domain.model;

public enum OutputType {
    CSV,
    XML,
    JSON
}
