package tcmb.webservice.domain.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum AggregationType {

    AVERAGE("avg"),
    MIN("min"),
    MAX("max"),
    FIRST("first"),
    LAST("last"),
    SUM("sum");

    private final String type;

    public static String fromAggregation(String aggregation){
        return Stream.of(values())
                .filter(value-> value.name().equals(aggregation))
                .map(AggregationType::getType)
                .findFirst()
                .orElse("avg");
    }
}
