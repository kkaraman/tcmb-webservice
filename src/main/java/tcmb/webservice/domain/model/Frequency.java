package tcmb.webservice.domain.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum Frequency {
    DAILY(1),
    WORK_DAY(2),
    WEEKLY(3),
    TWICE_IN_MONTH(4),
    MONTHLY(5),
    THREE_MONTHS(6),
    SIX_MONTHS(7),
    YEARLY(8);

    private final Integer frequencyCode;

    public static Integer fromFrequency(String frequency){
        return Stream.of(values())
                .filter(value-> value.name().equals(frequency))
                .map(Frequency::getFrequencyCode)
                .findFirst()
                .orElse(1);
    }
}
