package tcmb.webservice.domain.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tcmb.webservice.domain.model.AggregationType;
import tcmb.webservice.domain.model.Formula;
import tcmb.webservice.domain.model.Frequency;
import tcmb.webservice.domain.model.OutputType;
import tcmb.webservice.domain.model.Series;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRateRequest {
    Series series;
    LocalDate startDate;
    LocalDate endDate;
    OutputType outputType;
    String decimalSeparator;
    AggregationType aggregationType;
    Formula formula;
    Frequency frequency;
}
