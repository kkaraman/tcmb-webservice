package tcmb.webservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tcmb.webservice.client.ExchangeRateClient;
import tcmb.webservice.domain.model.response.ExchangeRateResponse;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExchangeRateService {

    private final ExchangeRateClient exchangeRateClient;

    public ExchangeRateResponse getExchangeRates(String series,
                                                 String startDate,
                                                 String endDate,
                                                 String frequency,
                                                 String type) {
        var target = exchangeRateClient.getExchangeRates(
                series,
                startDate,
                endDate,
                frequency,
                type);
        return target;
    }
}
