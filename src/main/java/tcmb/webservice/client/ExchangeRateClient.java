package tcmb.webservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import tcmb.webservice.client.api.ExchangeRateApi;
import tcmb.webservice.config.FeignConfig;

@FeignClient(
        name = "exchange-rate-service",
        url = "${tcmb-client.url}",
        configuration = FeignConfig.class)
public interface ExchangeRateClient extends ExchangeRateApi {
}
