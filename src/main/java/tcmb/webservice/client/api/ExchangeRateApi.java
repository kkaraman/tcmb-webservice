package tcmb.webservice.client.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tcmb.webservice.domain.model.response.ExchangeRateResponse;

public interface ExchangeRateApi {

    @RequestMapping(
            produces = "application/json",
            method = RequestMethod.GET,
            headers = {"key=${tcmb-client.key}"}
    )
    ExchangeRateResponse getExchangeRates(@RequestParam("series") String series,
                                          @RequestParam("startDate") String startDate,
                                          @RequestParam("endDate") String endDate,
                                          @RequestParam("frequency") String frequency,
                                          @RequestParam("type") String type);
}