package tcmb.webservice;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@EnableFeignClients
public class TcmbWebService {

	public static void main(String[] args) {
		SpringApplication.run(TcmbWebService.class, args);
	}
}
