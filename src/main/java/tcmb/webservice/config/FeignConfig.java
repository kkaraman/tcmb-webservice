package tcmb.webservice.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    public RequestInterceptor urlInterceptor() {
        return requestTemplate -> requestTemplate.uri(requestTemplate.url().replace("?",""));
    }
}
